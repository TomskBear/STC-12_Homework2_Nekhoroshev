package javaee.tomskbear;

class Main {
    public static void main(String[] args) {
        MathBox b = new MathBox();

        long summaResult = b.summa(1,2);
        System.out.println("summa result = " + summaResult);

        long diffResult = b.diff(1,2);
        System.out.println("diff result = " + diffResult);

        long factorialResult = b.factorial(5);
        System.out.println("factorial result = " + factorialResult);

        double dividerResult = b.dividerExceptionInside(1, 2);
        System.out.println("divider result = " + dividerResult);

        try {
            dividerResult = b.divider(1,2);
        }
        catch (Exception ex){
            System.out.println("We catch exception from divider");
            ex.printStackTrace();
        }

        try {
            dividerResult = b.dividerCustomException(1,2);
        }
        catch (CustomException ex){
            System.out.println("We catch Custom exception from divider");
            ex.printStackTrace();
        }
    }
}
