package javaee.tomskbear;

class MathBox {

    public int summa (Integer a, Integer b) {
        return a + b;
    }

    public int diff (Integer a, Integer b) {
        return a - b;
    }

    public long factorialStackOverflow(Integer a) {
        return factorialStackOverflow(a);
    }

    public long factorial (Integer a) {
        if (a >= 0) {
            long result = 1;
            for (int i = 1; i <= a; i++) {
                result *= i;
            }
            return result;
        }
        return 0;
    }

    public double dividerExceptionInside(Integer a, Integer b)  {
        try {
            return a/(double)b;
        }catch (ArithmeticException ex) {
            ex.printStackTrace();
        }
        return 0;

    }

    public double dividerCustomException(Integer a, Integer b) throws CustomException {
        try {
            return a/(double)b;
        }catch (ArithmeticException ex) {
            ex.printStackTrace();
            throw new CustomException();
        }
        finally {
            System.out.println("Error during dividerCustomException");
            return 0;
        }

    }

    public double divider (Integer a, Integer b) throws Exception {
        return a/(double)b;
    }
}
