package javaee.tomskbear;

class CustomException extends Exception {
    @Override
    public String getMessage() {
        String parentMessage = super.getMessage();
        String myMessage = "This is my custom message for demo";
        StringBuilder builder = new StringBuilder();
        builder.append(myMessage).append(Character.SPACE_SEPARATOR).append(parentMessage);
        return builder.toString();
    }
}
